#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState
from std_srvs.srv import Trigger, TriggerResponse
from std_msgs.msg import Bool
from tf.transformations import euler_from_quaternion
import time
import math
import os
import requests
import subprocess
import atexit

class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

class GameController:

    def __init__(self, robot_name, isZeroGRiderB):
        self.isZeroGRiderB = isZeroGRiderB
        self.distance_consider_feedback_working = 0.8
        self.frames_to_count_as_locked = 50

        self.docking_position = Vector3D(0.0, -24.0, -0.3)

        self.dock_sub = rospy.Subscriber("dock",Bool,self.dockingHandler)
        self.dock_state = False
        self.contact_sub = rospy.Subscriber("contact",Bool,self.contactHandler)
        self.contact_state = False

        self.robot_name = robot_name
        self.robot_status = "Docking"

        self.time_passed = 0.0
        self.start_time = 5.0


        self.bUserUsedVel = False

        self.metrics = {
            'Status' : self.robot_status,
            'Time' : self.time_passed,
        }

        self.git_dir = '/workspace/src/.git'
        commit_id = ''
        riders_project_id = os.environ.get('RIDERS_PROJECT_ID', '')

        # RIDERS_COMMIT_ID
        git_dir_exists = os.path.isdir(self.git_dir)
        if git_dir_exists:

            popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE)
            remote_url, error_remote_url = popen.communicate()

            popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            branch_name, error_branch_name = popen.communicate()

            popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            commit_id, error_commit_id = popen.communicate()

            if error_remote_url is None and remote_url is not None:
                remote_url = remote_url.rstrip('\n').rsplit('@')
                user_name = remote_url[0].rsplit(':')[1][2:]
                remote_name = remote_url[1]

        self.result_metrics = {
            'Status' : self.robot_status,
            'Time' : self.time_passed,
            'commit_id': commit_id.rstrip('\n'),
        }

        rospy.init_node("game_controller")

        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        self.init_services()

        self.send_api_check = False

        resp = self.get_robot_state("module", "")
        old_delta = Vector3D(0,0,0)
        old_yaw = -math.pi * 0.5

        count_x_transits = 0 # for checkpoint 1

        count_frames_in_x_zone = 0
        count_frames_in_z_zone = 0
        count_frames_in_yaw_zone = 0 # for B
        count_frames_in_pitch_zone = 0 # for B

        # update these when we are not dockd - save the final before docking value
        speed_error = 0
        distance_error = 0
        angle_error = 0

        while ~rospy.is_shutdown():
            # check if we are in range
            resp = self.get_robot_state("module", "")
            p = resp.pose.position

            q = resp.pose.orientation
            (roll, pitch, yaw) = euler_from_quaternion([q.x, q.y, q.z, q.w])

            if self.isZeroGRiderB:
                if yaw < -math.pi * 0.5 and old_yaw >= -math.pi * 0.5:
                    self.achieve(149)

            vel = resp.twist.linear
            
            pos = Vector3D(p.x, p.y, p.z)
            delta = pos - self.docking_position
            total_distance = delta.length()

            # print 'delta', delta.x, delta.y, delta.z
            if (delta.x > 0 and old_delta.x <= 0) or (delta.x < 0 and old_delta.x >= 0):
                count_x_transits += 1
                if count_x_transits == 2:
                    if not self.isZeroGRiderB:
                        self.achieve(138)

            # we check angular.z is not extreme - if spinning fast we're just getting random yaw points
            if self.isZeroGRiderB:
                # hacky - this is the right angle if you don't move from start point and look straight at the green dot
                # used to pass checkpoint 2
                good_pitch = 0.0531117736235
                good_yaw = -math.pi * 0.5 + 0.101599437397
                good_angular_error = 0.03
                delta_yaw = yaw - good_yaw
                delta_pitch = pitch - good_pitch
                while delta_yaw > math.pi:
                    delta_yaw -= 2.0 * math.pi
                while delta_yaw < -math.pi:
                    delta_yaw += 2.0 * math.pi
                while delta_pitch > math.pi:
                    delta_pitch -= 2.0 * math.pi
                while delta_pitch < -math.pi:
                    delta_pitch += 2.0 * math.pi
                if abs(delta_yaw) < good_angular_error:
                    count_frames_in_yaw_zone += 1
                else:
                    count_frames_in_yaw_zone = 0
                if abs(delta_pitch) < good_angular_error:
                    count_frames_in_pitch_zone += 1
                else:
                    count_frames_in_pitch_zone = 0

                if count_frames_in_yaw_zone == self.frames_to_count_as_locked:
                    self.achieve(150)
                    if not self.bUserUsedVel:
                        self.achieve(172)
                if count_frames_in_pitch_zone == self.frames_to_count_as_locked:
                    self.achieve(151)
                    if not self.bUserUsedVel:
                        self.achieve(173)

            if delta.x < self.distance_consider_feedback_working and delta.x >= -self.distance_consider_feedback_working:
                count_frames_in_x_zone += 1
                if count_frames_in_x_zone == self.frames_to_count_as_locked:
                    if not self.isZeroGRiderB:
                        self.achieve(138) # in case they did it right first time - count this for 1st checkpoint too
                        self.achieve(139)
                        if not self.bUserUsedVel:
                            self.achieve(166)
            else:
                count_frames_in_x_zone = 0

            if delta.z < self.distance_consider_feedback_working and delta.z >= -self.distance_consider_feedback_working:
                count_frames_in_z_zone += 1
                if count_frames_in_z_zone == self.frames_to_count_as_locked:
                    if not self.isZeroGRiderB:
                        self.achieve(141)
                        if not self.bUserUsedVel:
                            self.achieve(167)
            else:
                count_frames_in_z_zone = 0

            
            if self.robot_status != "Crashed":
                if delta.length() > 0.0001: # not docked hack
                    # distance_error is the lateral component since we snap front back
                    distance_error = math.sqrt(delta.x * delta.x + delta.z * delta.z)
                    speed_error = Vector3D(vel.x, vel.y, vel.z).length()

                    self.time_passed = rospy.get_time() - self.start_time
                    if self.time_passed < 0:
                        self.time_passed = 0

                    # make an error from our yaw and pitch
                    # this might fail if they rolled ...
                    # what is toolbox for getting vector from quat?
                    # then could do this with vector(0,1,0) to get a norm error
                    # out of time tonight ...
                    pitch_error = pitch - 0
                    yaw_error = yaw - (-math.pi * 0.5)
                    while pitch_error < -math.pi:
                        pitch_error += 2.0 * math.pi
                    while pitch_error > math.pi:
                        pitch_error -= 2.0 * math.pi
                    while yaw_error < -math.pi:
                        yaw_error += 2.0 * math.pi
                    while yaw_error > math.pi:
                        yaw_error -= 2.0 * math.pi
                    angle_error = math.sqrt(pitch_error * pitch_error + yaw_error * yaw_error)
                else:
                    self.robot_status = "Docked"

            total_speed = Vector3D(vel.x, vel.y, vel.z).length()

            old_delta = pos
            old_yaw = yaw

            self.publish_metrics(total_speed, total_distance, speed_error, distance_error, angle_error) # speed_error and distance_error ignored until we dock

            rate.sleep()

    def init_services(self):
        rospy.Service('/user_used_velocity',Trigger,self.user_used_vel)
        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.get_robot_state("module", "")
            if not resp.success == True:
                self.robot_status = "no_robot"

        except (rospy.ServiceException, rospy.ROSException), e:
            self.robot_status = "no_robot"
            rospy.logerr("Service call failed: %s" % (e,))

    def publish_metrics(self, speed, distance, speed_error, distance_error, angle_error):
        self.metrics['Status'] = self.robot_status
        self.metrics['Time'] = self.time_passed # round(self.time_passed, 2)
        self.metrics['Distance'] = distance
        
        if self.robot_status == "Docked" or self.robot_status == "Crashed":
            if self.robot_status == "Crashed":
                self.metrics['Speed'] = speed # show speed we are tumbling
            else:
                self.metrics['Speed'] = 0.0 # show as exactly 0

            if self.isZeroGRiderB:
                self.achieve(152)
                if not self.bUserUsedVel:
                    self.achieve(174)
            else:
                self.achieve(140)
                if not self.bUserUsedVel:
                    self.achieve(168)

            self.metrics['Speed Error'] = speed_error
            self.metrics['Distance Error'] = distance_error
            if self.isZeroGRiderB:
                self.metrics['Angle Error'] = angle_error
            if not self.isZeroGRiderB and self.robot_status == "Docked":
                if self.time_passed < 20.0 and speed_error < 0.1 and distance_error < 0.1:
                    self.achieve(148)
        else:
            self.metrics['Speed'] = speed # show speed we are still trying to dock
        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def send_to_api(self, score=0.0, disqualified=False,  **kwargs):
        simulation_id = os.environ.get('RIDERS_SIMULATION_ID', None)
        args = score, disqualified
        try:
            if simulation_id:
                self.send_simulation_results(simulation_id, *args, **kwargs)
            else:
                self.send_branch_results(*args, **kwargs)
        except Exception as e:
            rospy.loginfo('Exception occurred while sending metric: %s', e)

    def contactHandler(self,contact):
        if contact.data:
            self.robot_status = "Crashed"

    def dockingHandler(self,dock):
        pass # need address lag delay - check in controller for position actually updated
        #if dock.data:
        #    self.robot_status = "Docked"

    def user_used_vel(self,req):
        if not self.bUserUsedVel:
            self.bUserUsedVel = True
            print('got event: User used vel x')
        return TriggerResponse(
            success = True
        )
        

    def send_simulation_results(self, simulation_id, score, disqualified, **kwargs):
        host = os.environ.get('RIDERS_HOST', None)
        token = os.environ.get('RIDERS_SIMULATION_AUTH_TOKEN', None)
        create_round_url = '%s/api/v1/simulation/%s/rounds/' % (host, simulation_id)
        kwargs['score'] = kwargs.get('score', score)
        kwargs['disqualified'] = kwargs.get('disqualified', disqualified)
        data = {
            'metrics': json.dumps(kwargs)
        }
        # send actual data
        requests.post(create_round_url, {}, data, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


    def send_branch_results(self, score, disqualified, **kwargs):
        rsl_host = 'http://localhost:8059'
        # metrics endpoint stores data differently compared to simulation endpoint,
        # hence we change how we send it
        info = {
            'score': score,
            'disqualified': disqualified,
        }
        info.update(kwargs)
        url = '%s/events' % rsl_host
        data = {
            'event': 'metrics',
            'info': json.dumps(info)
        }

        requests.post(url, {}, data, headers={
            'Content-Type': 'application/json',
        })


    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
